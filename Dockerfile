FROM openjdk:21-jdk

WORKDIR /app

COPY /target/catalogue_management_system-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

CMD ["java", "-jar", "catalogue_management_system-0.0.1-SNAPSHOT.jar"]