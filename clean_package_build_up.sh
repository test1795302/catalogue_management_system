#!/bin/bash

mvn clean package -DskipTests

docker build -t spring_app:latest .

docker-compose up