package com.example.catalogue_management_system

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class CatalogueManagementSystemApplication

fun main(args: Array<String>) {
    runApplication<CatalogueManagementSystemApplication>(*args)
}
