package com.example.catalogue_management_system.Repository

import com.example.catalogue_management_system.model.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository: CrudRepository<Product, Long> {
    fun findByProductId(productId: Long) : Product
}