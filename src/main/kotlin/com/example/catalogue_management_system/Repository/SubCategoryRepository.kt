package com.example.catalogue_management_system.Repository

import com.example.catalogue_management_system.model.Category
import com.example.catalogue_management_system.model.SubCategory
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SubCategoryRepository: CrudRepository<SubCategory, Long> {
    fun findByCategory(categoryId: Category) : Iterable<SubCategory>
}