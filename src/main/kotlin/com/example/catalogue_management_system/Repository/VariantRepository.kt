package com.example.catalogue_management_system.Repository

import com.example.catalogue_management_system.model.Variant
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VariantRepository: CrudRepository<Variant, Long>