package com.example.catalogue_management_system.Repository

import com.example.catalogue_management_system.model.Category
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository: CrudRepository<Category, Long> {
    fun findByCategoryId(categoryId: Long) : Category
}