package com.example.catalogue_management_system.model

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.Table

@Entity
@Table(name = "variants")
data class Variant (
    @Id
    @GeneratedValue

    val variantId: Long? = null,
    val price: Double,
    val color: String,
    val description: String,
    val dimensions: String,
    val weight: Double,

    @ManyToOne
    @JoinColumn(name = "product_id")
    val product: Product
) {
    constructor(): this(null, 0.0, "", "", "", 0.0, Product())
}