package com.example.catalogue_management_system.model

import jakarta.persistence.*
import java.awt.Dimension
import javax.management.Descriptor

@Entity
@Table(name = "products")

data class Product (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val productId : Long? = null,

    val name : String,
    val description: String,
    val price: Double,
    val brand: String,
    val rating: Double = 0.0,

    @ManyToOne
    @JoinColumn(name = "sub_category_id")
    val subCategory: SubCategory? = null,

) {
    constructor(): this(null, "", "", 0.0, "",  0.0, null)
}