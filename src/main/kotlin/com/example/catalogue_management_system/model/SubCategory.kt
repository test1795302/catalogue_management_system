package com.example.catalogue_management_system.model

import jakarta.persistence.*
import org.jetbrains.annotations.NotNull

@Entity
@Table(name = "sub_categories")
data class SubCategory (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val subCategoryId: Long? = null,

    val name : String,

    @ManyToOne()
    @JoinColumn(name = "category_id")
    var category: Category,

) {
    constructor() : this(null, "", Category())
}