package com.example.catalogue_management_system.model

import jakarta.persistence.*

@Entity
@Table(name = "categories")
data class Category (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val categoryId: Long? = null,
    val label: String,
    val description: String,

) {
    constructor() : this(null, "", "")
}