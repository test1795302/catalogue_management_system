package com.example.catalogue_management_system.Service

import com.example.catalogue_management_system.Repository.CategoryRepository
import com.example.catalogue_management_system.Repository.SubCategoryRepository
import com.example.catalogue_management_system.model.SubCategory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SubCategoryService (@Autowired private val subCategoryRepository: SubCategoryRepository, @Autowired private val categoryRepository: CategoryRepository) {

    fun getAllSubCategory() : Iterable<SubCategory> {
        return subCategoryRepository.findAll()
    }

    fun getSubCategories(categoryId: Long) : Iterable<SubCategory> {
        val category = categoryRepository.findByCategoryId(categoryId)
        return subCategoryRepository.findByCategory(category)
    }

    fun saveSubCategory(subCategory: SubCategory): SubCategory {
        return subCategoryRepository.save(subCategory)
    }
}