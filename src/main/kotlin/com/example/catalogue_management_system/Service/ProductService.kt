package com.example.catalogue_management_system.Service

import com.example.catalogue_management_system.Repository.ProductRepository
import com.example.catalogue_management_system.model.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductService(@Autowired private val productRepository: ProductRepository) {

    fun getAllProducts(): Iterable<Product> {
        return productRepository.findAll()
    }

    fun getProductById(productId: Long): Product {
        return productRepository.findByProductId(productId)
    }

    fun saveProduct(product: Product) : Product {
        return  productRepository.save(product)
    }

    fun updateProduct(product: Product) : Product {
        return productRepository.save(product)
    }

    fun deleteProduct(productId: Long) : Unit {
        return productRepository.deleteById(productId)
    }
}