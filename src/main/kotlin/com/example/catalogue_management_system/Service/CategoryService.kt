package com.example.catalogue_management_system.Service

import com.example.catalogue_management_system.Repository.CategoryRepository
import com.example.catalogue_management_system.model.Category
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CategoryService (@Autowired private val categoryRepository: CategoryRepository) {

    fun getAllCategories(): Iterable<Category> {
        return categoryRepository.findAll();
    }

    fun createNewCategory(category: Category): Category {
        val newCategory: Category = categoryRepository.save(category)
        return newCategory
    }
}