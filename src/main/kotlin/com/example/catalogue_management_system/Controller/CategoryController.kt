package com.example.catalogue_management_system.Controller

import com.example.catalogue_management_system.Service.CategoryService
import com.example.catalogue_management_system.model.Category
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/category")
class CategoryController (@Autowired private val categoryService: CategoryService){

    @GetMapping("")
    fun getAllCategories(): ResponseEntity<Iterable<Category>> {
        val categories = categoryService.getAllCategories()
        return ResponseEntity(categories, HttpStatus.OK)
    }

    @PostMapping("")
    fun createNewCategory(@RequestBody category: Category): ResponseEntity<Category> {
        val createdCategory = categoryService.createNewCategory(category)
        return ResponseEntity(createdCategory, HttpStatus.CREATED)
    }

    //TODO: write category delete endpoint
}