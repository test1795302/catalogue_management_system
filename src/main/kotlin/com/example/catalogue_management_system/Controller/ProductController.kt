package com.example.catalogue_management_system.Controller

import com.example.catalogue_management_system.Service.ProductService
import com.example.catalogue_management_system.model.Product
import jakarta.websocket.server.PathParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpRange
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
class ProductController (@Autowired private val productService: ProductService) {

    @GetMapping("")
    fun getAllProducts(): ResponseEntity<Iterable<Product>> {
        var products = productService.getAllProducts()
        return ResponseEntity(products, HttpStatus.OK)
    }

    @GetMapping("/{productId}")
    fun getProduct(@PathVariable productId: Long) : ResponseEntity<Product>{
        var product = productService.getProductById(productId)
        return ResponseEntity(product, HttpStatus.OK)
    }

    @PostMapping("")
    fun createNewProduct(@RequestBody product: Product) : ResponseEntity<Product> {
        var product = productService.saveProduct(product)
        return ResponseEntity(product, HttpStatus.CREATED)
    }

    @PutMapping("/{productId}")
    fun updateProduct(@RequestBody product: Product) : ResponseEntity<Product> {
        var product = productService.updateProduct(product)
        return ResponseEntity(product, HttpStatus.OK)
    }

    @DeleteMapping("/{productId}")
    fun deleteProduct(@PathVariable productId : Long) : ResponseEntity<HttpStatus> {
        var deleted = productService.deleteProduct(productId)
        return ResponseEntity(HttpStatus.OK)
    }
}