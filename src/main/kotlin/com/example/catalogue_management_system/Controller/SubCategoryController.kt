package com.example.catalogue_management_system.Controller

import com.example.catalogue_management_system.Service.SubCategoryService
import com.example.catalogue_management_system.model.Category
import com.example.catalogue_management_system.model.SubCategory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/category")
class SubCategoryController (@Autowired private val subCategoryService: SubCategoryService) {

    @GetMapping("/sub-categories")
    fun getAllSubCategories(): ResponseEntity<Iterable<SubCategory>> {
        val subCategories = subCategoryService.getAllSubCategory()
        return ResponseEntity(subCategories, HttpStatus.OK)
    }

    @GetMapping("/{categoryId}/sub-categories")
    fun getSubCategories(@PathVariable categoryId: Long): ResponseEntity<Iterable<SubCategory>> {
        val subCategories = subCategoryService.getSubCategories(categoryId)
        return ResponseEntity(subCategories, HttpStatus.OK)
    }

    @PostMapping("/sub-categories")
    fun createNewSubCategory(@RequestBody subCategory: SubCategory): ResponseEntity<SubCategory> {
        val subCategory = subCategoryService.saveSubCategory(subCategory)
        return ResponseEntity(subCategory, HttpStatus.CREATED)
    }

}